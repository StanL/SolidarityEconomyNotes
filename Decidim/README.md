# Decidim

[Decidim](https://decidim.org/) is a self described *political technical*
project developing a platform for democractic participation.

## Links

* [The Government of Catalonia, Barcelona City Council and the Free Software Association Decidim sign a collaboration agreement to promote democratic participation with free and open technologies](https://decidim.org/blog/2021-02-09-the-government-of-catalonia-barcelona-city-council-and-the-free-software-association-decidim-sign-a-collaboration-agreement-to-promote-democratic-participation-with-free-and-open-technologies/?utm_source=meta.decidim.org&utm_campaign=newsletter_227)
* [Decidim, free open-source participatory democracy for cities and organizations](https://www.youtube.com/watch?v=M1MNwu4t7w0&t=7s) - Pablo Aragón 2019-05-28
