# US Tech Worker Coop Meeting Notes 2021-05-03

## Present: Colombene (CA), Nurul (IL), Jeff (VA)

Nurul met Micky at Platform Coops conference in NYC in 2018.  Backgroud in
Enterprise operations and program management.  Now co-owner / co-founder at
[ChiCommons.coop](https://www.chicommons.coop/)

Jeff is a longtime activist and HS CS teacher and Free Open Source software
advocate.  [NOVA Web Development Coop](https://novawebdevelopment.org/)
co-founder.
  
Colombene worked in design and tech for entertainment companies. Long
interested in politics, a few years ago she started looking for alternatives.
Involved in [Data Commons Coop](https://datacommons.coop/). She met Micky at
USFCW in LA.

## Unmet / Met Needs/ Goals:

* Where do we go from here: US Tech Worker CoOps?
* A democratic and better world
* Defend against oligarchy
* Federate producers for solidarity and power
* Incorporate independents
* Escape the horrors of corporate wage slavery
* Ease the pain of connecting and networking pro-social groups and projects
* Learn and explore tools like Decidim
* Using tech tools to faciliate the creation of human communites
* List tech worker coops in a sustainable way
* Model the way to "change rules of game" beyond proprietary thinking and
  exploitative platforms
* Work the inside and  outside game re: USFWC
* Ally with aligned/ alignable groups and individuals (workers, clients,
  service providers, etc)
* Get beyond the "languish" stage to sustain financially
* Aid in client and prospect education
* Address more complex client needs/ projects
* Explore being "value added resellers" for tools like Decidim, learn by doing
* Learn from experienced long-lived co-ops


## POTENTIAL NEXT STEPS

1. Populate the USTWC decidim instance content to be less cryptic
1. Content should answer: 
    * Why, Who, What, When, Where, How
    * Why = values / goals
    * What = economic and human ways
1. Combo of Wiki and interactive tool   

explore values goals -> explore cultural support & structures for that
explore economic & organizational needs -> explore approaches to meet them


## Tech coop ecosystem needs

* know which coops exists - in all stages and what services they offer
* organize tech workers interested in joining this ecosystem and get to know
  their skills
* support each other in finding & collaborating on work

## Who can be a part of network?

* tech co-ops (products, agencies)
* individual practitioners

## Look into (temp) code of conduct

* [Mission Protocol](https://missionprotocol.org/codeofconduct/)
* [Allied Media Projects Network Principles](https://alliedmedia.org/network-principles)
