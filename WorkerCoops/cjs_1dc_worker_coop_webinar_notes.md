# Quotes from Claudia Jones School / One DC Worker Co-op Webinar

"There was a time in the 19th century, and even in the early part of the 20th
century when the labor movement and the movement for cooperatives saw
each other as allies ... fighting against the same capitalist system, the
workers to get a better deal from the capitalists and the people in the co-ops
to go beyond capitalism."
R. Wolff [11:07]

"The socialist movement in the United States ... which is reviving now and
growing quickly ... is in a position to look at the worker co-op movement as
its proper ally, its proper coalition partner."
R. Wolff [15:28]

"Capitalism has had its day. It is declining in this part of the world. That's
our problem and the socialist movement, if it builds around the worker co-op
movement, will be a very powerful force for positive social change."
R. Wolff [16:28]

"I want to highlight that early period. The 1870s, 1880s, 1890s, where labor
and the cooperative movements started in US history."
J. Gordon Nembhard [17:22]

"Labor has to reclaim its sovereignty. We can't achieve true economic democracy
without worker ownership and worker control over the economy."
J. Gordon Nembhard [17:52]

"We've seen that it's not even enough to say that we've got government
ownership and reduce private ownership. We need to talk about the power and
sovereignty of labor, and that's where worker co-ops come in, where worker
ownership comes in.
J. Gordon Nembhard [18:32]

"In the 20th and 21st century co-op movement the co-ops aren't strong enough 
if they don't also have that labor ownership component. So just having a
consumer co-op is not as strong as having a worker co-op or having what we
now call a multi-stakeholder co-op where workers and consumers own the co-ops
together."
J. Gordon Nembhard [18:52]

"So another piece we have to talk about is not just changing captialism but
how important and crucial worker co-ops are in the cooperative and socialist
movements for those reasons that we need to understand worker ownership,
worker management, that the sovereignty of labor is crucial here."
J. Gordon Nembhard [19:13]

"What I can add is that worker cooperatives and multi-stakeholder cooperatives,
we don't have to be too strict here, cooperatives or organizations where
workers share ownership and decision making, and the decision making is the
most important one, are a vehicle to make socialism more democratic in the
economic sphere and also allows the state to concentrate on those areas that
are fundamental."
C. Piñiero Harnecker [19:40]

"Worker cooperatives allow [us] to have a different kind of socialism where
there is more participation in the economy."
C. Piñiero Harnecker [20:39]

"Unfortunately [the] previous experience of socialism understood social
ownership as state ownership. We have to broaden [that] and see what Marx
and other people who studied what would be this alternative society to
captialism should look like."
C. Piñiero Harnecker [20:43]

"Marx and Lenin and other marxists didn't see socialism as an all powerful
state that manages all enterprises. They saw the state just as a vehicle to
get to a non-state society. So how come in the socialism that we have seen
in countries the state has retained the management of so much of the economy?"
C. Piñiero Harnecker [21:08]

"Cooperatives are kind of a solution to solve this problem. The role that I see
for cooperatives beyond that is also as schools for citizenship [to develop]
the type of men and women we want to have in a socialist society."
C. Piñiero Harnecker [21:37]

"Cooperatives are a perfect space to learn about solidarity, to learn about
collective decision making, to learn about respecting and tolerating other
different types of views. I think that is another key role that cooperatives
can play for socialism."
C. Piñiero Harnecker [22:22]

"International policy is very important in international networking."
J. Gordon Nembhard [23:26]

"The US Federation of Worker Cooperatives is only 15 years old now. We couldn't
start it, we didn't start it without the help of CICOPA, that's the
International Organization of Industrial and Service Cooperatives, and the
Canadian Federation of Worker Cooperatives. They are the two organizations that
actually helped us to form our worker co-op organization. The US was the very
last "developed" country in the world to have a national federation of worker
cooperatives."
J. Gordon Nembhard [23:30]

"[CICOPA] works very closely with the ILO, International Labor Organization,
and the UN. In 2012 the UN called it the International Year of Cooperatives and
then had a decade of cooperatives, which ends in another year or so."
J. Gordon Nembhard [24:29]

"Connecting of mission, theory, and practice is really important at an
international level. Getting international research done, getting resources to
specific countries that need help that aren't as strong as other countries,
and then sharing best practices I think are really important."
J. Gordon Nembhard [25:07]

"The United States has to admit in this area as in quite a few others that it
is far behind other parts of the world in moving in the direction of co-ops.
Co-ops are a regular part of people's lives in other parts of the world that
we can learn from. Not to copy, of course we are a unique place. We'll have to
make adjustments for our situation, but let me just mention two or three where
we have something to learn."
R. Wolff [25:43]

"In Italy, the province around Bologna, Emilia-Romagna, this is a place, large
part of the Italian society, in which roughly 35 to 40 percent of the
businesses are work co-ops, and they've been that way for decades. That means
they have learned how to exist in a society where the majority is still
capitalist, without losing their identity, without losing their particularity,
keeping the mass of people solidly in favor of a cooperative economy that is
that large. It means in practice you have something extraordinary which we in
America can use. You have the possibility for the people of Emilia-Romagna,
whoever they are, to find out firsthand what the difference is between working
in a top-down capitalist enterprise, and what that's like, and what that means
in your life, versus being part of a worker co-op, where you don't just have
a particular task, but you're part of what runs the enterprise, what makes all
the big decisions. So people really have freedom of choice there in something
as the quality of their work life, something we do not have in the United
States, because we don't have a worker co-op sector that's big enough yet to
really be something that everybody can learn about and then make an informed
choice around."
R. Wolff [26:04]

"I think also of the Mondragon corporation. They've developed their own
university where they teach courses how to finance a worker co-op, how to
build one, how to handle the personnel problems that arise. Many of the issues
that I get questions about literally every day, they have whole courses
designed how to answer those questions. They are in a position to help us
with guidance and suggestions, things that have worked, things that have not
worked."
R. Wolff [27:34]

"The last example I will give you is Germany. In Germany cooperative banking is
very old and very powerful. There's a lot to be learned in how collective
cooperative banks organize by and for people. We have a little of that in this
country around the credit unions who have experimented with some of these
things. Let's build on that. Let's go and learn from the Germans. Every time I
talk to those people, they are not only willing, they are eager to provide
these kinds of interactions with the United States."
R. Wolff [28:11]

"I think that reaching out is a way for us to catch up on the backwardness that
this country still has, with the exception of the few, the courageous few, who
over the decades have built worker co-ops. I mean no disrepect. They had a hard
row to hoe and did a good job, but we can benefit from the international
advanced work that has been done." 
R. Wolff [28:50]

"Solidarity beyond [the] cooperative are the sixth and seventh of the universal
cooperative principles. The importance of intercooperation means cooperating
with other cooperatives. The seventh principle involves serving and being
committed to the development of the community."
C. Piñiero Harnecker [29:18]

"If you are members of a cooperative and you are not already a member of the
US Federation of Worker Cooperatives I would highly suggest that you look into
becoming a member. It's very affordable and it's a way for you to link with
cooperative movements in other countries because they are part of CICOPA."
C. Piñiero Harnecker [29:48]

"For cooperatives to be true cooperatives they need to work with each other.
There are very good examples in the US like credit unions which have their own
federation."
C. Piñiero Harnecker [30:22]

"I'm not aware of cooperatives coming together to get productive services
together. For example, instead of each cooperative having an accountant
separately, you can hire an accountant together. There are many advantages of
intercooperation that I don't think are being used in the US worker cooperative
movement that would highly benefit it, because it's very hard to fight against
the system by yourself. You can do so much more, not just to fight but to
survive and to grow if you are working together with other cooperatives."
C. Piñiero Harnecker [30:42]

"For me worker co-ops are the best chance we have that the socialism of the
21st century will not make the mistakes that the socialism of the 20th century
did. Let me be clear here. I think that socialism of the 19th and 20th
centuries was profoundly successful. I want to remind everyone that socialism
really didn't exist until early in the 19th century, so we barely have 200
years of it. In 200 years, that socialism, of the 19th and 20th century, went
into every country in the face of this world. There is no place where there
aren't socialists who are active in one way or another doing one kind of work
or another, and I am amazed at such a growth. Very few examples of human
history have anything like it. Obviously, socialism spoke to something very
deep, and very universal, all over the world."
R. Wolff [32:12]

"This is not a Monday morning quarterback criticism of what happened, but that
kind of socialism did what earlier speakers have talked about. It thought that
the government, and grabbing the government, whether it's through a revolution
or through an election, would be the way to carry then the next step and to go
forward, perfectly reasonable idea, a strategy that emerged in the 19th and
20th century, and it's why there are socialist governments around the world to
this day, many more than most Americans are every taught to understand. But
giving that much power to the government also brought with it terrible
mistakes, terrible conflagrations, lessons that socialists need to learn not to
do again."
R. Wolff [33:19]

"How do you make the state do what we as socialists wanted it to - take us to
a better system - rather than to become the substitute for the private
capitalist, so that all we've changed is the boss is from a private person now
the boss is a government official. That's not what we were after. We were after
much bigger fish than that."
R. Wolff [34:16]

"How do we do that? I think the answer is the worker co-op, because the worker
co-op puts the worker where the power is. They will control the money that
comes out of production. They will be in charge of the goods and services. The
government will have to listen not just to the workers, who vote and all of
that, but to the workers a second time, because the workers are going to be the
runners and the owners businesses too."

"The government won't be sold to the capitalists the way it is in our society.
There won't be any of them. It'll be the same working people on both sides of
the equation, and the government will have to listen to them. It will not be
able to serve a minority at the expense of a majority. I don't see anything
other than worker co-ops as the new foundation of the economy that can give us
the benefits of what socialism did, without the failures and mistakes that
socialism made."
R. Wolff [35:10]

"So for me, 21st century socialism is, I believe, going to be
centrally based on worker co-ops. It's going to be the foundation upon which
the other parts of socialism can rest without going into that dead end of
too much government power at the expense of everyone else. So for me, it's a
central part of socialism's future.
R. Wolff [35:45]

"In the Communist Manifesto, the way [Marx and Engles] thought about this new
society was as the free association of laborers or producers united or guided
by a common plan.  It's important to realize that most of their idea of what
this post-capitalist society would look like was based on what happened with
the Paris commune, where there were basically worker cooperatives producing
what needed to be done, but not in isolation, in coordination."
C. Piñiero Harnecker [36:22]

"What Lenin said right before dying - the regime of educated or cooperative
members, that is socialism."
C. Piñiero Harnecker [37:12]

"We have to see the role of worker cooperatives as central for true socialism."
C. Piñiero Harnecker [37:22]

"Worker co-ops are essential to combat racism, because some of the most
important aspects of the social construction of race are about labor. We
created a notion of race to justify enslavement, to justify enslaving a group
of people who had similar skin color and then we decided that they were an
inferior race and that made it okay to exploit them as workers."
J. Gordon Nembhard [37:49]

"The only way we can really address racism at the root is to also address this
class exploitation issue, and the fact that people of color are associated with
being exploitable. We didn't even own our own bodies at first, then we've been
alienated from capital for all these years. We've been exploited as workers.
If we are even thinking not just about socialism in general, but about what
does racial equity look like and how do we stop institutional and over racism,
worker co-ops are part of that solution."
J. Gordon Nembhard [38:27]

"We've got to control our own bodies as laborers. We have to control our own
work or we'll never be able to break out of the racist society that we're in.
In fact, we usually talk about racialized capitalism because it was built on
this notion of difference, that some people were more exploitable. Exploitation
is acceptable if you believe some people are exploitable.
J. Gordon Nembhard [39:04]

"We are rightly concerned with inequality in ownership and equality in wealth
and income. Worker cooperatives are a tool to make that transition. Some people
see them as just a way to make capitalism more humane. Cooperatives have
existed since the beginning of capitalism, so we know that cooperatives by
themselves are not going to get us to socialism. They're just part of the road
there."
C. Piñiero Harnecker [39:42]

"In fact, in countries like Yugoslavia [cooperatives] contributed to producing
or increasing social and ethnic tensions. That's one of the risks of
cooperatives, that if they are just focused on the group, looking inwards and
the people that are 'like me' instead of embedded in a culture of solidarity,
of transcending exploitation, discrimination, and all the evils of capitalism,
they're not going to get us to socialism. We have to be honest about it."
C. Piñiero Harnecker [40:32]

"[Cooperatives] are a great tool for solving what is at the root of many of the
ills of capitalism, which is not owning the means of production. What gives
you power? Owning the means of production. We are not going to solve racism,
we're not going to solve patriarchy, if women and people of color are not going
to have economic power."
C. Piñiero Harnecker [41:15]

"It is always a good time for solidarity. I think even when there is a crisis
it is more needed. It's just that this administration is doing things to
basically suffocate Cuba worse than any other president in history. There have
been some vehicles for worker co-op solidarity between the US and Cuba, and I
can talk about that later. The most important thing that you can do if you are
in solidarity with cooperatives in Cuba is to get Trump out of office and to
end the embargo or blockage against Cuba."
C. Piñiero Harnecker [42:16]

"[Emile Schepers] mentioned how worker cooperatives in Cuba operate in the
context of a supportive and friendly state, and here in the U.S. if worker
cooperatives are successfull they will be met with hostility and sabotage
because of our captialist society. [He] asked what are the implication of this,
and it seems like [he] kind of answered that, but I don't know if you want to
go further into that and answer that question?
JoJo Morinvil [43:26]

"So that Rick can add some of his ideas before he leaves we should talk about
what is the necessary ecosystem in a legal and regulatory environment that is
needed for cooperatives to grow. Do you want to start, Rick, and then I can
talk about what is being done in Cuba?"
C. Piñiero Harnecker [44:01]

"Let me offer something that I've learned over the last several years before
the virus hit as I travelled all over the country giving talks. Here's what I
learned. A huge number of people in the United States, I don't even hesitate to
say, a majority, if you get a chance to sit down in a relatively quiet place
and talk about the difference between a worker co-op and how it works, and a
capitalist hierarchical top-down alternative, you will discover, as I did, that
most Americans have no idea what a worker co-op is, and when you present it,
especially if you can do something with real examples, both in the United
States and outside, and how it works, and what some are, no need to sugarcoat
it, you will discover that Americans are very interested in it."
R. Wolff [44:15]

"Let me tell you about an experiment that was made. A former student of mine is
now the professor at the University of Rhode Island. They take students from
that university to Cuba, every year, to study Cuban cooperatives. These are
American students who come back excited beyond words at what they saw, what
they were able to learn, what they discussed with the co-op members who are
Cuban citizens."
R. Wolff [45:14]

"It was for me another example that this has to be our job. We have to take to
the mass of people, like Jessica did, the history of the co-ops, so it doesn't
seem to be something that dropped out of the sky. This is something that
American communities have worked with, have built, have succeeded and failed,
but there's a history here, as well as a very positive alternative to what they
see as their own future."
R. Wolff [45:48]

"I think we're in much shape thinking forward about taking this message across
the United States then we might have expected, and I don't think there is very
much Mr. Trump or the Republicans can do it if we do our work. Sure, they'll
throw in obstacles, but they're busy funding their own wealth accumulation. We
have a bit of a chance right now for all kinds of reasons."
R. Wolff [46:18]

"Let's run with that ball, because I think a worker co-op is a very powerful
way to talk to the American people. It's not what they think about when they
hear the word socialism. They think of this as a very interesting idea that
they're going to turn over in their head. Later on we can talk to them about
what it's got to do with socialism, but we don't have to worry about that.
Attitudes towards socialism are changing anyway in America in a good way, by
and large, and the worker co-op only makes that appeal stronger."
R. Wolff [46:47]

"Even though I share, of course, the horror at what the American government is
and does these days, but we are in better shape to move forward with a
socialist program and a worker co-op program than we have been for quite a
while. At least that's how it appears to me as I encounter the response of the
American people." 
R. Wolff [47:25]

"I was going to add the link for [The Center for Global
Justice](https://www.globaljusticecenter.org/). They are based in Oaxaca
Mexico, but they are mostly expats from the US. They organize every year a trip
to Cuba to learn about the cooperatives there."
C. Piñiero Harnecker [47:50]

"Also I was going to say the [Human Agenda](http://www.humanagenda.net/) in
California. They also very often have trips to Cuba."
C. Piñiero Harnecker [48:11]

"I have to say that for this legal regulatory environment and support ecosystem
that is needed for cooperatives to expand, Cuba is not the best example to look
at. I have to be honest. We are very critical and it is far from what it needs
to be, because it's starting from this very state-centric understanding of
socialism."
C. Piñiero Harnecker [48:25]

"Fidel's program, when they were arguing for the need to end the Batista
dictatorship and what kind of society needed to be built in Cuba, from the
very beginning talked about worker cooperatives as a solution for land
concentration, for injustice in the rural areas, but also as a solution for
more democratic businesses too."
C. Piñiero Harnecker [48:55]

"Unfortunately, although at the beginning there were many cooperatives, like
cooperatives of workers who would manage the sugar industries and consumer
cooperatives, there were many attempts at cooperatives, but this path was not
followed."
C. Piñiero Harnecker [49:23]
 
"The only cooperatives that have been with us since the beginning of the
revolution are the agricultural cooperatives. We only began to allow for
cooperatives in other sectors of the economy in 2012. We are in the process of
setting the regulatory or the support ecosystem.
C. Piñiero Harnecker [49:41]

"I encourage people to look at the experience of Uruguay and Paraguay. Those
are the best examples of nurturing ecosystems for cooperatives."
C. Piñiero Harnecker [50:04]

"I do think that in the US, I know that in the US people don't like to look at
other countries because, it's true, the US is kind of like several countries
all together, but maybe at the state level, I know in New York, in the bay
area, there is already a lot that has been done, but I do think that it is
important, especially for worker cooperatives to grow, to have public policies
that promote."
C. Piñiero Harnecker [50:17]

"Also something in the US I don't think people are paying enough attention to
is pedagogical supervision of cooperatives, because the minute you start having
public policies that promote cooperatives, you have people creating fake
cooperatives to take advantage of the preferential treatment, and then you get
the movement all confused about what a cooperative really is. This doesn't
help. It can even take you back many years and a lot of progress."
C. Piñiero Harnecker [50:55]

"You already have representation, which is key. You need also to coordinate
among all these different stakeholders. Those are the four main functions of
an enabling ecosystem in my view."
C. Piñiero Harnecker [51:32]

"I know there is a lot of work in regulation. I do think because in the US it's
very easy to create businessess it's not taken into consideration, but for
those studying worker cooperatives you know that the law is not going to make
you have good cooperatives. There are people breaking the rules all time, but
if you don't have a law that is enabling for worker cooperatives, it's going to
be much harder for people to create good cooperatives, true cooperatives."
C. Piñiero Harnecker [51:46]

"In terms of the international solidarity and studying, what I found in the
Black community was actually similar studying of different success stories
and learning from other groups. They went to the [Antigonish
Movement](https://en.wikipedia.org/wiki/Antigonish_Movement) in Nova Scotia
and studied what was happing with [Father
Cody](https://en.wikipedia.org/wiki/Moses_Coady) with co-op education. One of
the black groups in the 30s brought one of the major cooperators from Japan to
come and speak to Black communities to talk about how co-ops are working in
Japan."
J. Gordon Nembhard [52:18]

"Blacks have started going to Emilia-Romagna and Mondragon, so there are really
interesting ways of learning best practices, seeing how other people do it. The
[Federation of Southern Cooperatives](https://www.federation.coop/), which is
a Black regional co-op development association, mostly rural, has actually, at
least before Trump, had all kinds of exchanges, education and best practices,
with Cuban co-ops. One of the things that was important there was that they
weren't just doing cultural exchanges, they were also doing trade, helping
each other's businesses."
J. Gordon Nembhard [53:02]

"International trade is also very important, buying from each other, selling
each other supplies, parts, keeping that supply chain within the co-op
movement has been important."
J. Gordon Nembhard [53:43]

"The Black co-ops also studied each other. I have examples of the group in
Memphis in the 1920s visiting the group in Gary, Indiana, and learning from
them or the group in Chicago going to Gary, and visiting the Richmond people.
They were all learning from what each other were doing."
J. Gordon Nembhard [53:54]

"Those study groups, study circles, study tours, learning journeys are also
really important both for the education and the exchange of best practices, but
also when you can, to do that exchange of supplies and do the supply chain.
That also helps strengthen the businesses not just on the education side but
on the business side."
J. Gordon Nembhard [54:18]

"Richard, I know you have to leave, so feel free to sign off. Thank you so
much for joining us and answering these questions and participating in the
discussion. It's amazing to have you here and we learned so much from you."
JoJo Morinvil [54:49]

"Well it's very very kind of you to say. Let me return the favor. You're
doing what I said before. You, by this evening, gathering us, gathering the
people who are participating at the other end. You're spreading the word.
You're getting people to think about and think around all that worker
co-ops can mean, the kinds of contributions that they can make. That's the
work that needs to be done. So thank you all, and thank the institution that
is hosting this and all the people behind the scenes. I feel very strengthened
that you're all working the way I am, and you know it just makes me feel very
very good that this kind of activity, this kind of an event is now happening,
and that we're utilizing this strange, weird time in American history to push
forward in this way. So my hat's off to you very much, very much. So thank
you all for the opportunity, and I hope to see you all at a future point."
R. Wolff [55:02]

"Part of the issue [behind the State of West Virginia stopping the
co-op at the Bluefields Colored Institute] was that the state of West Virginia
in the 1920s didn't want Black co-ops to flourish, and even though this was
just a student co-op, it was showing a great example of how students could
run their own student stationary store on the campus of a Black high school.
One of the things that this is an example of is that Black owned co-ops were
undermined. White competitors, sometimes governmental state and federal
apparatus, would put Black co-ops out of business, make it hard for the Black
co-ops to exist, found a reason to accuse them of not following the law
properly. There are lot of cases, and this is one of them, where I don't have
all the details of why this happened and why the government shut down this
co-op. I just have what W.E.B. Du Bois said about it, but often there were
examples after examples where Blacks were accused of not following the law
properly and so their co-ops where shut down. Other things were done too.
The banks colluded not provide loans or lines of credit. Sometimes federal
agencies wouldn't give mortgages or loans. So there is this collusion, this
sabotage, this not allowing the Black co-ops."
J. Gordon Nembhard [57:03]

"It's not just government, it's a white supremacist government wanting
not wanting Blacks to do things. Often what Black communities did was they
went underground and they still continued to do what they wanted to do. They
just couldn't do it through an official business. So they went underground
and they called it something else. That's why again we have this problem in
history of not knowing the history, because often our families, our leaders
stopped calling it a co-op if the co-op was going to get undermined or called
illegal, or fire bombed. You stop calling it a co-op. You might still do it,
you might still cooperate, but you did it underground. You didn't tell your
children what you were doing. You called it the store even though it was
still operating sort of as a co-op. That's the other problem here. You end up
getting thigs put under the rug, called different names, so that later people
have to resurrect the history."
J. Gordon Nembhard [58:40]

"Someone just sent me a notice that a woman in Champaign, Illinois uncovered
her family's farming and co-op history. It turns out her family owned a farm
and was part of a farm co-op, and started a whole co-op society in Champaign
county. She hadn't know anything about it for years. One of the reasons she
said she never knew about it sooner was because her family was ashamed of
having been farmers, and they focused more on their relationship to the
University of Illinois and Champaign instead of this wonderful history about
organizing a co-op and co-op society. So it took her years to uncover that
history. That's the kind of thing, whether it's actual state violence against
the co-op or other kinds of challenges that ends up making our history more
invisible."
J. Gordon Nembhard [59:49]

"That's one of the issues, but there's another issue. Actually, I think Camila
already talked about it, the need for enabling laws and a co-op ecosystem to
really be supportive. If you don't have co-op laws that are strong enough,
flexible enough, especially to support worker co-ops, then it's harder for
these co-ops exist, to stay in existance. It's harder to teach people about
them because they have to be done more informally, so they don't have that
structure and background and supportive system."
J. Gordon Nembhard [1:00:49]

"It's one of the reasons why right now the US worker co-op movement is actually
arguing for uniform national co-op laws, especially for worker co-ops. Right
now cooperative incorporation is state by state. In some states you can only
incorporate agricultural co-ops and credit unions. Some states have laws for
worker co-ops, but they're not very good or very strong. So there is this
issue about needing more uniform stable and flexible laws to allow for all
the different kinds, but especially for worker co-ops, and then to make sure
to have some kind of protection."
J. Gordon Nembhard [1:01:29]

"The final thing I would say about the protection issue is the two most
important ways we can protect our co-ops are: one, the education that we've all
been talking about. We need to understand how they work, how to make them
strong, and how to do collective decision making."
J. Gordon Nembhard [1:02:11]

"The other think I found in history is the co-op that lasted the longest
were protected by their communities, even the people who weren't members,
people who knew that the co-op was what it was understood why it had started,
what its purpose was, and protected it in the community. Protected it either
with their bodies when the Klu Klux Klan were trying to burn it down or lynch
their leaders, or protecting it by being customers, donating money, you know,
any kind of support. In all these examples, if you don't have community
support, if you don't have people who understand what the project is, you
don't have those interconnections, that's what is going to be the real
failure."
J. Gordon Nembhard [1:02:30]

"I already said that we have agricultural cooperatives in Cuba from the
beginning of the revolution. That was part of the two agrarian law reforms.
That's how the state was able to deconcentrate land ownership which, I don't
know what percentage, but at least 30 percent was in US companies like United
Fruit. The best lands in Cuba were owned by US corporations. That's why the
US government hasn't liked the Cuban revolution among other reasons."
"[Agricultural cooperatives] have been in existance since then.
C. Piñiero Harnecker [1:04:00]

"Something important to mention too, because it was part of the question for
Jessica about what to do when businesses are failing, [is the situation in
Cuba that happened during the special period].  In this case they were
state farms, that due to the fall of the Soviet Union and not having access to
all the agricultural inputs that were imported from the communist bloc, and not
having the ability to export to the communist bloc, a lot of the big state
farms failed. So the decision was taken to divide them up into what they
called basic production units. At the beginning they were hybrid, but they
really were cooperative, and now had the same autonomy and decision making as
other agricultural cooperatives."
C. Piñiero Harnecker [1:04:49]

"The lesson here is that cooperatives are a way when you have failing
enterprises to make sure that if the workers are interested in taking on the
reigns of their own workplaces, they can do so. It was a way to save
agricultural production in Cuba, one of the components of how Cuba got out of
that crisis where there was basically little agricultural production because
of the lack of fertilizers and all that. So it got us out of this hole."
C. Piñiero Harnecker [1:05:54]

"In Cuba there were many problems in the early 90s with lack of adequet
nutrition, so there were some health issues related to inadequet nutrition,
but there was no starvation, like in China or in other countries, and that's
thanks to these cooperatives."
C. Piñiero Harnecker [1:06:41]

"In 2012 as part of what is called the 'actualization', which began in 2005
when Fidel called for the rethinking of socialism. It was the first time he
did a public speech recognizing the problems of our socialist model and
calling on us to rethink different ways to address the problems we have."
C. Piñiero Harnecker [1:07:05]

"In 2008 there were some measures that we took that preceeded the decision to
allow for cooperatives to be created outside of agricultural in Cuba in 2012.
It is part of the process of actualization that had a lot of public input.
There were three rounds of consulations. We have a new constitution that was
passed last year and approved. We have a model of what kind of society we want
to build, which is called conceptualization, and the first thing we had was
these guidelines to say 'these are the problems we have, this is what we want
to do', and one of the decisions was we want to create cooperatives outside
of agriculture. We want higher tier cooperatives."
C. Piñiero Harnecker [1:07:05]

"Now we have in addition to the agricultural cooperatives, which number around
5000 cooperatives, we have around 500 non-agricultural cooperatives,
cooperatives outside of agriculture. These new cooperatives have aroud 17,500
members. These are 99 percent worker cooperatives, with one that is on the
border between being a producer and worker cooperative. In total, we have more
than half a million cooperative members, putting together agricultural and
non-agricultural cooperatives. Cooperatives are around 10 percent of the labor
force, and there are many different estimates of how much they contribute to
the GDP, but in Cuba all these economic calculations are very complicated due
to the dual currency, and different exchange rates, so if you consider that
agricultural cooperatives produce between 80 and 90 percent of agricultural
output, that gives you an understanding of how important cooperatives are.
They manage close to 70 percent of the land. So they produce more with the
amount of land that they have in comparison to the state."
C. Piñiero Harnecker [1:08:45]

"What I can say is that cooperatives have been seen as part of the solution
to update Cuban socialism."
C. Piñiero Harnecker [1:10:18]

"What we want is to improve our socialist system. We don't want the revolution
to end. We don't want all that has been accomplished by our revolutionary
process to be lost. What we want is to make it better, and we want to make it
more sustainable. We want to make it more attractive, so that people really
feel they are owners, because state enterprises are great for providing health
care, universal and affordable education, many of the scientific efforts
where you need to make it large scale in order to make it affordable and
accessible, and high quality for everyone."
C. Piñiero Harnecker [1:10:40]

"People in many state enterprises don't feel like owners, and you don't need
the state to be managing services, and many light industries, so this has been
acknowledged, and what has been said is that the goal is to get from around 15
percent of non-state employment to around 45 or 50 percent."
C. Piñiero Harnecker [1:11:26]

"Cooperatives are expected to play a key role in this transfer of economic
activity away from the state and into the non-state sector. The unfortunate
part of it is that we don't yet have that general law of cooperatives that
will really allow cooperatives to grow. The almost 500 cooperatives that have
been created were part of an experiment. Because 75 percent came out of state
enterprises, the state wanted to have very close control of which enterprises
were converted into cooperatives, but that hindered the emergence of
cooperatives out of people's initiative. So of those 500 only 25 percent
come out of people's initiative, and we know there were more than 900
proposals in only a few months that were not taken into consideration."
C. Piñiero Harnecker [1:12:05]

"The summary is that we could have many more cooperatives in Cuba, and
hopefully as part of the approval of the new constitution and as part of the
measures that are being taken to combat the pandemic and the crisis that has
followed, this general law is going to be passed and cooperatives are going
to be able to grow and have at least part of this nurturing environment that
is needed."
C. Piñiero Harnecker [1:13:04]

"We have a nurturing, or at least preferential treatment for cooperatives that
is important to know. Cooperatives pay only half the taxes that a similar
private business would pay. Cooperatives have preferential bidding to lease
state property. Cooperatives can buy inputs at a discount. This is a big
bottleneck in the Cuban economy, because due to the embargo, or the blockade,
and if you don't know about that, do some research, it has been very damaging
for the Cuban economy and for Cubans as people, it is very hard in Cuba to
get any inputs for your house and for your business because you can not
import from the US very easily. You have to pay in cash. Cuba is not allowed
to use dollars. All these things have been done by the US government to make
the Cuban economy fail. They have made it as hard as possible for us to
operate. So it's very hard for a cooperative, or any other enterprise, to
access inputs, but the state made a commitment that even under these very
harsh circumstances they would try to make it accessible and affordable for
the cooperatives."
C. Piñiero Harnecker [1:13:39]

"So you can see that there are all these public policies, and also funding 
from the banks to the cooperatives, so there is an intention to [support them].
There are other problems, like middle level bureaucrats who see cooperatives
as a threat to their interests, but I think there is a decision to promote
cooperatives, and once this general law is passed, and once we have this
institute that promotes cooperatives, we are going to see cooperatives
flourish in Cuba, and hopefully by then Trump is not going to be there and
we can have all kinds of exchanges and learn from each other." 
C. Piñiero Harnecker [1:15:10]

"Are these times ripe for worker ownership? Do we see more employees buying
out their companies, transforming them more democratically? Yes. First of all
we're already seeing a rise in mutual aid happening, groups just getting
together and helping each other, and working through each other. There's also
a strong interest in beginning business conversions to worker co-ops. Some
co-op developers have actually put out marketing stating that if you're
having trouble, think about selling to your employees. We already, and this
is before COVID, the US has actual tax credits for sole proprietors to sell to
their employees, so that it's a benefit if they do that, and then there's help
for the employees to buy the business from the employeer. So we already have
that infrastructure luckily in place."
J. Gordon Nembhard [1:16:10]

"So now with the COVID pandemic and another great recession, which I think is
probably going to be a great depression if it's not a depression already, we
have another moment in time when the capitalist system is not working, does
not serve during the pandemic, and cooperatives, especially worker ownership
could be a way to salvage economic activity. We know that when workers own
their companies productivity is higher. They get rid of the middle man so they
save costs in that sense. They usually use some kind of wage solidarity, so
wages go up, but the costs of labor don't go up, the two heads better than
one notion of more people figuring things out and figuring out how to do this,
the social energy and excitement that worker owners have to be a part of their
own company and to be running their own company, we have lot of evidence about
how productive worker co-ops are and can be, and how they can actually address
some of the market failure issues."
J. Gordon Nembhard [1:17:23]

"When capitalist companies can't make it work because they're trying to just
gouge everybody, pay as little as possible, and make as much profit as
possible, when you have worker co-op companies, they're doing a combination of
things. They want to make a high quality good or service. They want to do it in
a collective supportive way. They're not trying to gouge the labor, but they're
also trying to make sure that the community gets what they need, and they're
happy with a return that allows them to keep the business going."
J. Gordon Nembhard [1:18:31]

"There is research about how resilient co-ops are during crises, much more
resilient than regular small businesses. Actually, even wthout a crisis the
research shows that a co-op lasts longer than a regular small business or
sole proprietorship, again because of all the things I just said, and
because the worker co-op model is so flexible, you can move with the times,
but also during crises that flexibility, that connection to people helping
each other and decision making and working things out together also means
that co-ops survive. They're more resilient and they survive crises better."
J. Gordon Nembhard [1:19:06]

"The third piece is that co-ops seem to develop out of crises. Especially for
Black co-ops, but I think this is the same for all co-ops in the US, the most
prolific periods of time in US history when we had the most co-ops were
actually periods of crisis, the 1930s, the great depression, has one of the
largest number of Black co-ops that I could find, and I think it is the same
for co-ops in general. In the 1880s, another crisis period, there was a large
proliferation of worker co-ops and co-ops in general."
J. Gordon Nembhard [1:19:46]

"We also see that during crises people see that, one, realize that captialism
either created the crisis or isn't helping or isn't the way out of the crisis,
or they start co-ops just because they absolutely need some way to survive
and they know they already that solidarity economics and working together and
cooperating together is a way to survive. We keep doing it in all periods.
When we were enslaved we gardened together make extra food so we wouldn't be
malnurished, things like that. So we already know that in a crisis banding
together, pooling our resources together, sharing risks, make a difference,
and help us get out of the crisis."
J. Gordon Nembhard [1:20:22]

"So we naturally move back into the thing that is human, which is to work
together and help each other, and that makes a difference. So we're in the same
kind of situation. We already see people working together, banding together,
helping each other. We've seen it in the past, so this is a perfect time for
us to be really educating ourselves about what is a worker co-op. How to do it.
What can we do about it. Especially with COVID, maybe we can't have meetings
together, but we can certainly still study together. We can still study thes
models and do the learning that we need to do, and start rebuilding, taking
over companies building from the bottom up so that we can make the world
better."
J. Gordon Nembhard [1:21:04]

"I want to address a question in the Q&A box from Chelsea, who asked, What is
the role of labor unions in worker cooperatives?"
JoJo Morinvil [1:22:05]

"Go ahead, Camila."
J. Gordon Nembhard [1:21:04]

"No, you should follow the idea, because now many labor unions are using
cooperatives as a way of getting workers to be able to acquire the company, if
that is failing. You go first and then if I have anything to add I'll go."
C. Piñiero Harnecker [1:22:11]

"There is a strong a strong role, and there is a legacy of labor unions and
co-ops. Remember both Rick Wolff and I started out talking about that early
connections in the 1880s, where the labor unions were doing and talking about
cooperative ownership and starting worker co-ops, and their platform was not
just organizing labor to get better wages and better work, but actually to own
the means of production, to own their own companies."
J. Gordon Nembhard [1:22:35]

"As we went into the 20th century the labor unions started to become more
segragated, more conservative. They really kind of got co-opted, and became
much more the other side of the coin of corporatism. It was the way that
corporations and labor came to some kind of agreement to not try to eliminate
each other, but they would try to coexist a little bit. So then labor dropped
the co-op, and some of its militancy. It's also the time period, in the early
20th century, when labor became very racist, with divide an conquer, labor
didn't want Black workers to be in the labor union. That's what we inherited,
those of us now entering the 21st century, we inherited this notion that
labor was racist, that labor and co-ops don't mix. When the labor movement in
the US started out it wasn't racist. It was very progressive. It was all about
owning the means of production and worker co-ops."
J. Gordon Nembhard [1:23:11]

"We now do have some labor unions who have become interested again in
supporting co-ops and in supporting their co-op members, and we have worker
co-ops that are unionized. Some of the worker co-ops actually became worker
co-ops of their union drive. Their union drive politicized them. There's a
group called [Collective Copies](https://www.collectivecopies.com) in
Massachusetts. They did a labor drive first. They were workers at a national
copy center. They did a labor drive. They became unionized and then the copy
owners didn't want a unionized workforce and tried to sell the company, so
they decided to buy it themselves, and they stayed in their union. There is
[Lusty Lady](https://en.wikipedia.org/wiki/Lusty_Lady), strip club, peep show
in San Francisco, also the same thing, they started out with a labor drive,
wanted to be unionized so they could get better working conditions, better
wages. After they won, the owner didn't want a unionized shop, and so was
going to sell out from under them, and they realized that if they didn't buy it
themselves they were going to be stuck with another horrible owner, so they
bought the shop. Actually, the dancers, the janitors, and the bouncers bought
the shop together, and so showed a real kind of labor solidarity that you
don't normally see from different classes of labor in the same company. So
you have those kind of examples." 
J. Gordon Nembhard [1:24:22]

"Now you've got the Steelworkers Union working with Mondragon to start
cooperatives in Cincinnati and a couple of other cities, I believe New York.
The wobblies have always been supporting worker co-ops, and SEIU is supporting
worker co-ops now because one of their locals is actually our largest US
worker cooperative, [Cooperative Home Care Associates](http://chcany.org/),
which is mostly owned by Black and Latino women. So SEIU realized that the
union co-op model is a really important model for empowering labor. So there
are lots of good examples that."
J. Gordon Nembhard [1:25:54]

"I think there is a trend to hopefully get to that point where unions see
cooperatives as a way to really liberate workers from exploitation, which
should be the goal of unions, and it has been happening in many countries for
some time now."
C. Piñiero Harnecker [1:26:45]

"Something good to look at is what happened in Argentina with the bankruptcy
law. It is very key to have a bankruptcy law that allows workers to buy the
company with what the owners owe their workers. That's key to allow for these
worker conversions out of failing enterprises."
C. Piñiero Harnecker [1:27:09]

"The communist movement, the socialist movement all had a place for co-ops and
worker co-ops. They weren't featured as the major issue because both of those
movements early on got connected to government control over land, government
control over production, and didn't focus as much on worker control, but there
always was support for cooperatives in general, there always was the
connection. In fact, for the Black community a lot of the red baiting against
communism stopped people from doing cooperative ownership and being involved in
co-ops, because they didn't want to be red-baited. So there are lots of
connections."
J. Gordon Nembhard [1:28:34]

"I also want to bring up A. Philip Randolph, because he was a leader in the
early 20th century in the Black socialist party, in the Black socialist
movement. He was editor of the Black socialist magazine,
[The Messanger](https://en.wikipedia.org/wiki/The_Messenger_(magazine)), and
in 1918 he wrote an article about how important cooperatives were to African
Americans. For him there was a connection between socialism and cooperativism.
He saw it as being important to Black liberation, to Black economic prosperity
that we needed to be involved in cooperative economics. When he became founder
and president of the
[Brotherhood of Sleeping Car Porters](https://en.wikipedia.org/wiki/Brotherhood_of_Sleeping_Car_Porters),
the first official independent Black trade union, he and the ladies auxiliary
spent decades talking about cooperative economics and how important it was not
just to be unionized, but to use cooperatives to recirculate resources within
the Black community, and to make sure the good wages that unionized Blacks
were getting were then used to support Black businesses and the Black
community."
J. Gordon Nembhard [1:29:18]

"A. Philip Randolph, even though we don't know him as much as a Black
socialist, and we see him more for what he was doing in the 60s with the
[March on Washington](https://en.wikipedia.org/wiki/March_on_Washington_for_Jobs_and_Freedom),
he was very much a proponent of how important collective ownership and
democratic participation were for Black people."
J. Gordon Nembhard [1:30:52]

"It is interesting that he and Du Bois don't seem to be in conversation about
that even though they overlap in years, because Du Bois also argued the same
thing. Before he became a communist he was a communalist and co-operator. From
before the turn of the century he started studying Black co-ops and what he
called economic cooperatiion, and what we would now call solidarity economy
activies."
J. Gordon Nembhard [1:31:23]

"He continued to argue how important it is for African Americans to see
collective ownership as the way to prosperitiy. It was not enough for us to try
to get our piece of the pie, to become little capitalists. That wasn't the way
to develop and bring properity to the whole group. The only real way to do that
would be what he called broad ownership of wealth and collective ownership of
wealth."
J. Gordon Nembhard [1:31:51]

"He told about it in a variety of ways from the 1890s to the 1930s and 40s.
In 1903 in The Souls of Black Folk he said that the problem of the 20th
century was the problem of the color line, but in 1907, at the conference on
Black businesses and cooperatives that he organized in Atlanta, he said we're
standing at a crossroads, we are either going to go the route of individualist,
capitalist weath building, and a few of us will make it and the rest of us
will be left behind, or we can choose the cooperation route, we can work
together to amass wealth together, and the whole group of us will prosper."
J. Gordon Nembhard [1:32:18]

"He was basically saying it's our choice. We pretty much know what happened,
most people went the individualistic route."
J. Gordon Nembhard [1:33:04]

"At the turn of the 20th century he was talking not only about race being
an issue, but our economic structure being an issue, and what way forward
we were going to choose. We could say that again now."
J. Gordon Nembhard [1:33:10]

"I don't know a lot about his decision to join the CPUSA at the end of his
life, but my understanding was that he joined more for political reasons.
The US government was being so repressive, and he was so disgusted with the
McCarthy era of the 50s, so by the early 60s, even though the McCarthy era was
winding down, he was disgusted by how we were thinking about and treating
communism, and the Communist Party was the closes thing to talking about
collective ownership. I think he joined more to flip his nose at the US
government and the repressive ideas then anything else."
J. Gordon Nembhard [1:33:26]

"Even though it isn't what he's known for, if you read him you can see how
much he was promoting economic concerns. He also had arguments about this.
The NAACP didn't like his idea of a group cooperative economy, because they
said that was going back to self-segregation, and they didn't want to
self-segregate. Other Black civil rights people were afraid of being red
baited, so they didn't want to talk about co-ops."
J. Gordon Nembhard [1:34:25]

"In his autobiograph in 1940 he said maybe it was a mistake that he didn't
push co-ops enough, but the arguments got be socialism or communism or
captialism. Then there were the wars, World War I and World War II, and it
never seemed like the right time to push cooperatives."
J. Gordon Nembhard [1:34:58]

"So I think even though he was clear about how important cooperative were
economically he got caught up in a lot of different political battles, and
didn't keep consistently fighting the co-op battle."
J. Gordon Nembhard [1:35:12]

"In my book I have a lot of great quotes from him about how important
economic cooperation is, and how that's really what is going to free us,
make us properous, but also free us from the horrible exploitation,
marginalization, and dehumanization that we suffer."
J. Gordon Nembhard [1:35:32]

"I don't know how much the other communist parties, and I'm sorry I don't know
enough about the Communist Party in the US either, but I think that
unfortunately the understanding of social ownership of the means of production
in Cuba was state ownership."
C. Piñiero Harnecker [1:36:09]

"I think there was very little appreciation of the important role that
cooperatives should play in socialism or communism, the post capitalist
society, however we want to call it."
C. Piñiero Harnecker [1:36:58]

"I really value Che Guevara's contributions to the Cuban process and to
communist internationalism in the world, but I think that at least for Cuba
he looked at the Yugoslav self-managed enterprises, and he looked at the 
Soviet [cohoses??], and he thought that those were cooperatives, and so Che
was influential in the Communist Party's understanding of cooperatives in the
sense that he saw cooperatives as group ownership and not socialist ownership
because it was just about the group."
C. Piñiero Harnecker [1:37:23]

"This is really a challenge for cooperatives. That's why the principles of
intercooperation and commitment to the community are so key, but they are very
hard put into practice."
C. Piñiero Harnecker [1:38:25]

"In reality, cooperatives are group ownerships. The people who own and the
people who decide are the members. How do you get these people to see beyond
their interests, not just in a utilitarian way, but to understand that in
order to be successful you need the support of the community and you need to
intercooperate with others."
C. Piñiero Harnecker [1:38:40]

"How do you get them to see that in order to be liberated and to fully
develop all your skills and capacities, you also need to work with others to
change the system, to make sure that your interest as a human being and not
just as an owner or business manager is met."
C. Piñiero Harnecker [1:39:04]

"Che was keen to highlight this limitation of cooperatives, and I think rightly
so. The majority of cooperatives in the world are agricultural cooperatives and
credit unions. In some places they are truly having a transformational impact,
but in many cases they are just part of the system."
C. Piñiero Harnecker [1:39:40]

"We have to be honest in recognizing the limitations of cooperatives. That's
why I think it's so important for cooperatives to be part of higher tier
cooperatives and federations and part of the global solidarity movement, but
also for cooperatives, even worker cooperatives, to take into consideration
other interests. Look at who are being impacted by your activity and see how
you can bring them into the decision making."
C. Piñiero Harnecker [1:40:19]

"The idea of multi-stakeholder cooperatives, which Jessica mentioned earlier,
has been developed within the cooperative movement. This is a very important
model for consumer cooperatives and for producer cooperatives. In Cuba we have
producer cooperatives in which the managers and the technitians are members of
the cooperative. In US agricultural cooperatives you have the people you have
the people who own the farms, and they are the members. The manager is hired,
and the accountant is hired."
C. Piñiero Harnecker [1:40:51]

"We have to think about how cooperartives go beyond just narrow group
interests, to take into consideration broader social interests."
C. Piñiero Harnecker [1:41:52]

"Let me jump in here. Thanks Camila for mentioning that. That's why I've
started in the last five years or more talking about a solidarity cooperative
economy or a solidarity cooperative commonwealth because its not just
cooperative, but we need that solidarity part."
J. Gordon Nembhard [1:42:06]

"I noticed in the research I did on Black co-ops that most of the Black leaders
who talked about co-ops, and we haven't really talked about
[Fannie Lou Hamer](https://en.wikipedia.org/wiki/Fannie_Lou_Hamer)
yet or [Ella Baker](https://en.wikipedia.org/wiki/Ella_Baker), but even
[Du Bois](https://en.wikipedia.org/wiki/W._E._B._Du_Bois) and
[Randolph](https://en.wikipedia.org/wiki/A._Philip_Randolph) and the other
leaders who talked about co-ops really saw it as a solidarity system, not just
what the group of owners of one co-op could do, but the fact that we have
multiple groups of owners who then are working together, and we're all in it as
part of a community who wants to make life better for all of us."
J. Gordon Nembhard [1:42:06]

"Fannie Lou Hamer talked about cooperative ownership of land, meaning that the
whole community would be developed because it stops monopolizing land. Ella
Baker talked about how important it was for the people most effected, the
people most marginalized, to be able to speak for themselves, to be the owners
and the directors and the leaders, and that's what real democracy is about, and
that cooperatives are the economic version of that. We can move back and forth
between doing it politically and economically."
J. Gordon Nembhard [1:43:02]

"A lot of the talk in the Black community has been about the co-existance of
the political and the economic democracy and how important the broader mission
is, so it's not just for the group of owners, but because you're coming
together as a group of owners you're doing it for the better good of everybody
else. We have to see those interlocking systems in that way, but we do have
to be vigilent about that because it is easier sometimes for everyone to get
locked into their own co-op at the very local level."
J. Gordon Nembhard [1:43:41]


J. Gordon Nembhard [1:44:23]
